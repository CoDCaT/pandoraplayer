package com.example.codcat.pandoraplayer;

import android.content.Context;

import com.example.codcat.pandoraplayer.di.AppRepositoryComponent;
import com.example.codcat.pandoraplayer.di.DaggerAppComponent;
import com.example.codcat.pandoraplayer.di.DaggerAppRepositoryComponent;
import com.example.codcat.pandoraplayer.di.modules.PreferenceModule;
import com.example.codcat.pandoraplayer.di.modules.RealmModule;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;


public class App extends DaggerApplication {

    public static volatile AppRepositoryComponent repositoryComponent;
    public static volatile Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = getApplicationContext();
        buildDaggerComponents();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.create();
    }

    private void buildDaggerComponents() {
        repositoryComponent = DaggerAppRepositoryComponent.builder()
                .realmModule(new RealmModule(this))
                .preferenceModule(new PreferenceModule(this))
                .build();
    }
}
