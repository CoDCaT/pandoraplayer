package com.example.codcat.pandoraplayer.feature.player_screen;

import com.example.codcat.pandoraplayer.base.BasePresenter;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenContract;

import javax.inject.Inject;


public class PlayerScreenPresenter<V extends PlayerScreenContract.View> extends BasePresenter<V> implements PlayerScreenContract.Presenter<V> {

    @Inject
    public PlayerScreenPresenter(V mMvpView) {
        super(mMvpView);
    }

}
