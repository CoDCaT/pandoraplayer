
package com.example.codcat.pandoraplayer.data.pojo.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("stat")
    @Expose
    private String stat;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private String code;

    public String getStat() {
        return stat;
    }

    public Result getResult() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
