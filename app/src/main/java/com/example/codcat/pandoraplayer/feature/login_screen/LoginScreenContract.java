package com.example.codcat.pandoraplayer.feature.login_screen;


import com.example.codcat.pandoraplayer.base.MvpPresenter;
import com.example.codcat.pandoraplayer.base.MvpView;

public interface LoginScreenContract {

    interface View extends MvpView{

        void navigateToMainScreen();

    }

    interface Presenter<V extends MvpView> extends MvpPresenter<V> {

        void onAuthUser(String s, String toString);
    }
}
