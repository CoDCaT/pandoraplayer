package com.example.codcat.pandoraplayer.feature.result_screen;


import com.example.codcat.pandoraplayer.base.MvpPresenter;
import com.example.codcat.pandoraplayer.base.MvpView;

public interface ResultScreenContract {

    interface View extends MvpView{

    }

    interface Presenter<V extends MvpView> extends MvpPresenter<V> {

    }
}
