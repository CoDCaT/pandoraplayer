
package com.example.codcat.pandoraplayer.data.pojo.Responce;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ooyala {

    @SerializedName("streamingPercentage")
    @Expose
    private Integer streamingPercentage;
    @SerializedName("streamingWhitelist")
    @Expose
    private List<Integer> streamingWhitelist = null;
    @SerializedName("videoAdBufferRetryCount")
    @Expose
    private Integer videoAdBufferRetryCount;
    @SerializedName("videoAdLoadingTimeout")
    @Expose
    private Integer videoAdLoadingTimeout;
    @SerializedName("videoAdPlayTimeout")
    @Expose
    private Integer videoAdPlayTimeout;

    public Integer getStreamingPercentage() {
        return streamingPercentage;
    }

    public void setStreamingPercentage(Integer streamingPercentage) {
        this.streamingPercentage = streamingPercentage;
    }

    public List<Integer> getStreamingWhitelist() {
        return streamingWhitelist;
    }

    public void setStreamingWhitelist(List<Integer> streamingWhitelist) {
        this.streamingWhitelist = streamingWhitelist;
    }

    public Integer getVideoAdBufferRetryCount() {
        return videoAdBufferRetryCount;
    }

    public void setVideoAdBufferRetryCount(Integer videoAdBufferRetryCount) {
        this.videoAdBufferRetryCount = videoAdBufferRetryCount;
    }

    public Integer getVideoAdLoadingTimeout() {
        return videoAdLoadingTimeout;
    }

    public void setVideoAdLoadingTimeout(Integer videoAdLoadingTimeout) {
        this.videoAdLoadingTimeout = videoAdLoadingTimeout;
    }

    public Integer getVideoAdPlayTimeout() {
        return videoAdPlayTimeout;
    }

    public void setVideoAdPlayTimeout(Integer videoAdPlayTimeout) {
        this.videoAdPlayTimeout = videoAdPlayTimeout;
    }

}
