package com.example.codcat.pandoraplayer.feature.result_screen;

import com.example.codcat.pandoraplayer.base.BasePresenter;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenContract;

import javax.inject.Inject;


public class ResultScreenPresenter<V extends ResultScreenContract.View> extends BasePresenter<V> implements ResultScreenContract.Presenter<V> {

    @Inject
    public ResultScreenPresenter(V mMvpView) {
        super(mMvpView);
    }

}
