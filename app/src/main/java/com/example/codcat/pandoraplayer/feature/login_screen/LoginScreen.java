package com.example.codcat.pandoraplayer.feature.login_screen;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.codcat.pandoraplayer.R;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreen;
import com.example.codcat.pandoraplayer.utils.Helper;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class LoginScreen extends DaggerAppCompatActivity implements LoginScreenContract.View {

    @Inject LoginScreenContract.Presenter presenter;

    @BindView(R.id.et_Login) EditText etLogin;
    @BindView(R.id.et_Password) EditText etPassword;
    @BindView(R.id.btn_SignIn) Button btnSIgnIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        ButterKnife.bind(this);

        etLogin.setText(Helper.LOGIN);
        etPassword.setText(Helper.PASS);
        btnSIgnIn.setOnClickListener(this::onClickButton);
    }

    private void onClickButton(View view) {
        presenter.onAuthUser(etLogin.getText().toString(), etPassword.getText().toString());
    }

    @Override public void showLoading() {

    }

    @Override public void hideLoading() {

    }

    @Override public void onError(@NonNull String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override public void showMessage(@NonNull String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToMainScreen() {
        Intent intent = new Intent(this, MainScreen.class);
        startActivity(intent);
    }
}
