
package com.example.codcat.pandoraplayer.data.pojo.login_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    public User(Object existingAuthToken, Boolean keepLoggedIn, String password, String username) {
        this.existingAuthToken = existingAuthToken;
        this.keepLoggedIn = keepLoggedIn;
        this.password = password;
        this.username = username;
    }

    @SerializedName("existingAuthToken")
    @Expose
    private Object existingAuthToken;
    @SerializedName("keepLoggedIn")
    @Expose
    private Boolean keepLoggedIn;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("username")
    @Expose
    private String username;

    public Object getExistingAuthToken() {
        return existingAuthToken;
    }

    public void setExistingAuthToken(Object existingAuthToken) {
        this.existingAuthToken = existingAuthToken;
    }

    public Boolean getKeepLoggedIn() {
        return keepLoggedIn;
    }

    public void setKeepLoggedIn(Boolean keepLoggedIn) {
        this.keepLoggedIn = keepLoggedIn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
