package com.example.codcat.pandoraplayer.data.repository;


public interface IPreferences {

    interface SettingChangeListener {
        void changed();
    }

    void getIsUserAuthorization(String key, SettingChangeListener l);
    boolean getIsJsonParsed();
    void setIsUserAuthorization(boolean b);
}
