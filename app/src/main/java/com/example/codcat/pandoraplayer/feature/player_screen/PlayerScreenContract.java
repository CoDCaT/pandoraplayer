package com.example.codcat.pandoraplayer.feature.player_screen;


import com.example.codcat.pandoraplayer.base.MvpPresenter;
import com.example.codcat.pandoraplayer.base.MvpView;

public interface PlayerScreenContract {

    interface View extends MvpView{

    }

    interface Presenter<V extends MvpView> extends MvpPresenter<V> {

    }
}
