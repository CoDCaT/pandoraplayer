
package com.example.codcat.pandoraplayer.data.pojo.delete;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseLogin {

    @SerializedName("activeVxRewards")
    @Expose
    private List<Object> activeVxRewards = null;
    @SerializedName("adkv")
    @Expose
    private Adkv adkv;
    @SerializedName("allowProfileComments")
    @Expose
    private Boolean allowProfileComments;
    @SerializedName("artistAudioMessagesEnabled")
    @Expose
    private Boolean artistAudioMessagesEnabled;
    @SerializedName("artistPromoEmailsEnabled")
    @Expose
    private Boolean artistPromoEmailsEnabled;
    @SerializedName("authToken")
    @Expose
    private String authToken;
    @SerializedName("birthYear")
    @Expose
    private Integer birthYear;
    @SerializedName("config")
    @Expose
    private Config config;
    @SerializedName("emailOptOut")
    @Expose
    private Boolean emailOptOut;
    @SerializedName("explicitContentFilterEnabled")
    @Expose
    private Boolean explicitContentFilterEnabled;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("highQualityStreamingEnabled")
    @Expose
    private Boolean highQualityStreamingEnabled;
    @SerializedName("isNew")
    @Expose
    private Boolean isNew;
    @SerializedName("listenerId")
    @Expose
    private String listenerId;
    @SerializedName("listenerToken")
    @Expose
    private String listenerToken;
    @SerializedName("minor")
    @Expose
    private Boolean minor;
    @SerializedName("notifyOnComment")
    @Expose
    private Boolean notifyOnComment;
    @SerializedName("notifyOnFollow")
    @Expose
    private Boolean notifyOnFollow;
    @SerializedName("profilePrivate")
    @Expose
    private Boolean profilePrivate;
    @SerializedName("seenEducation")
    @Expose
    private Boolean seenEducation;
    @SerializedName("smartConversionAdUrl")
    @Expose
    private String smartConversionAdUrl;
    @SerializedName("smartConversionDisabled")
    @Expose
    private Boolean smartConversionDisabled;
    @SerializedName("smartConversionTimeoutMillis")
    @Expose
    private Integer smartConversionTimeoutMillis;
    @SerializedName("stationCount")
    @Expose
    private Integer stationCount;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("webClientVersion")
    @Expose
    private String webClientVersion;
    @SerializedName("webname")
    @Expose
    private String webname;
    @SerializedName("zipCode")
    @Expose
    private String zipCode;

    public List<Object> getActiveVxRewards() {
        return activeVxRewards;
    }

    public void setActiveVxRewards(List<Object> activeVxRewards) {
        this.activeVxRewards = activeVxRewards;
    }

    public Adkv getAdkv() {
        return adkv;
    }

    public void setAdkv(Adkv adkv) {
        this.adkv = adkv;
    }

    public Boolean getAllowProfileComments() {
        return allowProfileComments;
    }

    public void setAllowProfileComments(Boolean allowProfileComments) {
        this.allowProfileComments = allowProfileComments;
    }

    public Boolean getArtistAudioMessagesEnabled() {
        return artistAudioMessagesEnabled;
    }

    public void setArtistAudioMessagesEnabled(Boolean artistAudioMessagesEnabled) {
        this.artistAudioMessagesEnabled = artistAudioMessagesEnabled;
    }

    public Boolean getArtistPromoEmailsEnabled() {
        return artistPromoEmailsEnabled;
    }

    public void setArtistPromoEmailsEnabled(Boolean artistPromoEmailsEnabled) {
        this.artistPromoEmailsEnabled = artistPromoEmailsEnabled;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public Boolean getEmailOptOut() {
        return emailOptOut;
    }

    public void setEmailOptOut(Boolean emailOptOut) {
        this.emailOptOut = emailOptOut;
    }

    public Boolean getExplicitContentFilterEnabled() {
        return explicitContentFilterEnabled;
    }

    public void setExplicitContentFilterEnabled(Boolean explicitContentFilterEnabled) {
        this.explicitContentFilterEnabled = explicitContentFilterEnabled;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getHighQualityStreamingEnabled() {
        return highQualityStreamingEnabled;
    }

    public void setHighQualityStreamingEnabled(Boolean highQualityStreamingEnabled) {
        this.highQualityStreamingEnabled = highQualityStreamingEnabled;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public String getListenerId() {
        return listenerId;
    }

    public void setListenerId(String listenerId) {
        this.listenerId = listenerId;
    }

    public String getListenerToken() {
        return listenerToken;
    }

    public void setListenerToken(String listenerToken) {
        this.listenerToken = listenerToken;
    }

    public Boolean getMinor() {
        return minor;
    }

    public void setMinor(Boolean minor) {
        this.minor = minor;
    }

    public Boolean getNotifyOnComment() {
        return notifyOnComment;
    }

    public void setNotifyOnComment(Boolean notifyOnComment) {
        this.notifyOnComment = notifyOnComment;
    }

    public Boolean getNotifyOnFollow() {
        return notifyOnFollow;
    }

    public void setNotifyOnFollow(Boolean notifyOnFollow) {
        this.notifyOnFollow = notifyOnFollow;
    }

    public Boolean getProfilePrivate() {
        return profilePrivate;
    }

    public void setProfilePrivate(Boolean profilePrivate) {
        this.profilePrivate = profilePrivate;
    }

    public Boolean getSeenEducation() {
        return seenEducation;
    }

    public void setSeenEducation(Boolean seenEducation) {
        this.seenEducation = seenEducation;
    }

    public String getSmartConversionAdUrl() {
        return smartConversionAdUrl;
    }

    public void setSmartConversionAdUrl(String smartConversionAdUrl) {
        this.smartConversionAdUrl = smartConversionAdUrl;
    }

    public Boolean getSmartConversionDisabled() {
        return smartConversionDisabled;
    }

    public void setSmartConversionDisabled(Boolean smartConversionDisabled) {
        this.smartConversionDisabled = smartConversionDisabled;
    }

    public Integer getSmartConversionTimeoutMillis() {
        return smartConversionTimeoutMillis;
    }

    public void setSmartConversionTimeoutMillis(Integer smartConversionTimeoutMillis) {
        this.smartConversionTimeoutMillis = smartConversionTimeoutMillis;
    }

    public Integer getStationCount() {
        return stationCount;
    }

    public void setStationCount(Integer stationCount) {
        this.stationCount = stationCount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWebClientVersion() {
        return webClientVersion;
    }

    public void setWebClientVersion(String webClientVersion) {
        this.webClientVersion = webClientVersion;
    }

    public String getWebname() {
        return webname;
    }

    public void setWebname(String webname) {
        this.webname = webname;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}
