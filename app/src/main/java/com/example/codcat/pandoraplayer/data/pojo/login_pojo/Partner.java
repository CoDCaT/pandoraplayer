
package com.example.codcat.pandoraplayer.data.pojo.login_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Partner {

    public Partner(String version, String deviceModel, String password, String username) {
        this.version = version;
        this.deviceModel = deviceModel;
        this.password = password;
        this.username = username;
    }

    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("deviceModel")
    @Expose
    private String deviceModel;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("username")
    @Expose
    private String username;


    public String getVersion() {
        return version;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
