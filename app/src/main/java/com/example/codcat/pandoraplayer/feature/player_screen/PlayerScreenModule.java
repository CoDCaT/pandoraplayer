package com.example.codcat.pandoraplayer.feature.player_screen;

import com.example.codcat.pandoraplayer.di.scopes.ActivityScope;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreen;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenContract;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenPresenter;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class PlayerScreenModule {

    @Binds
    @ActivityScope
    abstract PlayerScreenContract.View view(PlayerScreen playerActivity);

    @Binds
    @ActivityScope
    abstract PlayerScreenContract.Presenter presenter(PlayerScreenPresenter<PlayerScreenContract.View> playerActivityPresenter);


}
