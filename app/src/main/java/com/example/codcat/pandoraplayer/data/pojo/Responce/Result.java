
package com.example.codcat.pandoraplayer.data.pojo.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("isAllowed")
    @Expose
    private boolean isAllowed;
    @SerializedName("stationSkipLimit")
    @Expose
    private Integer stationSkipLimit;
    @SerializedName("partnerId")
    @Expose
    private String partnerId;
    @SerializedName("partnerAuthToken")
    @Expose
    private String partnerAuthToken;
    @SerializedName("syncTime")
    @Expose
    private String syncTime;
    @SerializedName("deviceProperties")
    @Expose
    private DeviceProperties deviceProperties;
    @SerializedName("stationSkipUnit")
    @Expose
    private String stationSkipUnit;

    public Integer getStationSkipLimit() {
        return stationSkipLimit;
    }

    public void setStationSkipLimit(Integer stationSkipLimit) {
        this.stationSkipLimit = stationSkipLimit;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerAuthToken() {
        return partnerAuthToken;
    }

    public void setPartnerAuthToken(String partnerAuthToken) {
        this.partnerAuthToken = partnerAuthToken;
    }

    public String getSyncTime() {
        return syncTime;
    }

    public void setSyncTime(String syncTime) {
        this.syncTime = syncTime;
    }

    public DeviceProperties getDeviceProperties() {
        return deviceProperties;
    }

    public void setDeviceProperties(DeviceProperties deviceProperties) {
        this.deviceProperties = deviceProperties;
    }

    public String getStationSkipUnit() {
        return stationSkipUnit;
    }

    public void setStationSkipUnit(String stationSkipUnit) {
        this.stationSkipUnit = stationSkipUnit;
    }

    public boolean isAllowed() {
        return isAllowed;
    }

    public void setAllowed(boolean allowed) {
        isAllowed = allowed;
    }
}
