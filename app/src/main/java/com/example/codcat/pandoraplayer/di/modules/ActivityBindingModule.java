package com.example.codcat.pandoraplayer.di.modules;

import com.example.codcat.pandoraplayer.di.scopes.ActivityScope;
import com.example.codcat.pandoraplayer.feature.login_screen.LoginScreen;
import com.example.codcat.pandoraplayer.feature.login_screen.LoginScreenModule;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreen;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenModule;
import com.example.codcat.pandoraplayer.feature.player_screen.PlayerScreen;
import com.example.codcat.pandoraplayer.feature.result_screen.ResultScreen;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = MainScreenModule.class)
    abstract MainScreen bindMainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = LoginScreenModule.class)
    abstract LoginScreen bindLoginActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = LoginScreenModule.class)
    abstract PlayerScreen bindPlayerActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = LoginScreenModule.class)
    abstract ResultScreen bindResultActivity();


}
