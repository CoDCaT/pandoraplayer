
package com.example.codcat.pandoraplayer.data.pojo.delete;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Config {

    @SerializedName("branding")
    @Expose
    private String branding;
    @SerializedName("dailySkipLimit")
    @Expose
    private Integer dailySkipLimit;
    @SerializedName("experiments")
    @Expose
    private List<Integer> experiments = null;
    @SerializedName("flags")
    @Expose
    private List<String> flags = null;
    @SerializedName("inactivityTimeout")
    @Expose
    private Integer inactivityTimeout;
    @SerializedName("monthlyListeningCapHours")
    @Expose
    private Integer monthlyListeningCapHours;
    @SerializedName("stationSkipLimit")
    @Expose
    private Integer stationSkipLimit;

    public String getBranding() {
        return branding;
    }

    public void setBranding(String branding) {
        this.branding = branding;
    }

    public Integer getDailySkipLimit() {
        return dailySkipLimit;
    }

    public void setDailySkipLimit(Integer dailySkipLimit) {
        this.dailySkipLimit = dailySkipLimit;
    }

    public List<Integer> getExperiments() {
        return experiments;
    }

    public void setExperiments(List<Integer> experiments) {
        this.experiments = experiments;
    }

    public List<String> getFlags() {
        return flags;
    }

    public void setFlags(List<String> flags) {
        this.flags = flags;
    }

    public Integer getInactivityTimeout() {
        return inactivityTimeout;
    }

    public void setInactivityTimeout(Integer inactivityTimeout) {
        this.inactivityTimeout = inactivityTimeout;
    }

    public Integer getMonthlyListeningCapHours() {
        return monthlyListeningCapHours;
    }

    public void setMonthlyListeningCapHours(Integer monthlyListeningCapHours) {
        this.monthlyListeningCapHours = monthlyListeningCapHours;
    }

    public Integer getStationSkipLimit() {
        return stationSkipLimit;
    }

    public void setStationSkipLimit(Integer stationSkipLimit) {
        this.stationSkipLimit = stationSkipLimit;
    }

}
