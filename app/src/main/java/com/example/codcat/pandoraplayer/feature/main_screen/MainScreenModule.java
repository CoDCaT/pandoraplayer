package com.example.codcat.pandoraplayer.feature.main_screen;

import com.example.codcat.pandoraplayer.di.scopes.ActivityScope;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class MainScreenModule {

    @Binds
    @ActivityScope
    abstract MainScreenContract.View view(MainScreen mainActivity);

    @Binds
    @ActivityScope
    abstract MainScreenContract.Presenter presenter(MainScreenPresenter<MainScreenContract.View> mainActivityPresenter);


}
