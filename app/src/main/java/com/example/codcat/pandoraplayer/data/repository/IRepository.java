package com.example.codcat.pandoraplayer.data.repository;

import com.example.codcat.pandoraplayer.data.pojo.Responce.Response;

import io.reactivex.Single;
import io.realm.Realm;

public interface IRepository {

    Realm getRealm();
    Single<Response> getAuthData(String login, String password);

    Single<Response> isAllowedLicense();
}
