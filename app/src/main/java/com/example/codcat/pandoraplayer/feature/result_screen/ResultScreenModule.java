package com.example.codcat.pandoraplayer.feature.result_screen;

import com.example.codcat.pandoraplayer.di.scopes.ActivityScope;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreen;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenContract;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenPresenter;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class ResultScreenModule {

    @Binds
    @ActivityScope
    abstract ResultScreenContract.View view(ResultScreen resultActivity);

    @Binds
    @ActivityScope
    abstract ResultScreenContract.Presenter presenter(ResultScreenPresenter<ResultScreenContract.View> resultActivityPresenter);


}
