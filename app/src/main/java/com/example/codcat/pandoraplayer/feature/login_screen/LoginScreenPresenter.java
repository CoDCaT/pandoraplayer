package com.example.codcat.pandoraplayer.feature.login_screen;

import android.annotation.SuppressLint;

import com.example.codcat.pandoraplayer.App;
import com.example.codcat.pandoraplayer.base.BasePresenter;
import com.example.codcat.pandoraplayer.data.pojo.Responce.Result;
import com.example.codcat.pandoraplayer.data.repository.IPreferences;
import com.example.codcat.pandoraplayer.data.repository.IRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class LoginScreenPresenter<V extends LoginScreenContract.View> extends BasePresenter<V> implements LoginScreenContract.Presenter<V> {

    private IRepository repository;
    private IPreferences preferences;

    @Inject
    public LoginScreenPresenter(V mMvpView) {
        super(mMvpView);
        repository = App.repositoryComponent.getRepository();
        preferences = App.repositoryComponent.getAppPreferences();
    }

    @SuppressLint("CheckResult")
    @Override
    public void onAuthUser(String login, String password) {

//        if (isLicenseAllowed())

        Single.zip(
                repository.isAllowedLicense(),
                repository.getAuthData(login, password),
                ((license, response) -> {
                    boolean isAllowed = false;
                    if (license.getStat().equals("ok")) isAllowed = license.getResult().isAllowed();
                    else return null;
                    if (response.getStat().equals("ok")) response.getResult().setAllowed(isAllowed);
                    else return null;
                    return response;
                }))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (data != null) {
                        Result response = data.getResult();
                        if (!response.isAllowed())
                            getMvpView().showMessage("Не доступно в Вашем регионе");
                        else if (!response.getPartnerAuthToken().equals(""))
                            getMvpView().navigateToMainScreen();
                        else
                            getMvpView().onError(String.format("response cid: %s", data.getCode()));
                    }
                }, error -> getMvpView().showMessage(error.getMessage()));

        //TODO: Р—Р°РїРёСЃР°С‚СЊ С‚РѕРєРµРЅ РІ Р±Р°Р·Сѓ РёР»Рё Pref
//                    boolean token = data.getResult().getIsAllowed();
//                    if(token) getMvpView().navigateToMainScreen();

//                    boolean token = data.getResult().getIsAllowed();
//                    if(token) getMvpView().navigateToMainScreen();

    }

}
