package com.example.codcat.pandoraplayer.feature.login_screen;

import com.example.codcat.pandoraplayer.di.scopes.ActivityScope;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreen;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenContract;
import com.example.codcat.pandoraplayer.feature.main_screen.MainScreenPresenter;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class LoginScreenModule {

    @Binds
    @ActivityScope
    abstract LoginScreenContract.View view(LoginScreen loginActivity);

    @Binds
    @ActivityScope
    abstract LoginScreenContract.Presenter presenter(LoginScreenPresenter<LoginScreenContract.View> loginActivityPresenter);

}
