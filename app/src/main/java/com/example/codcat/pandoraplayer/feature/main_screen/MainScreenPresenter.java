package com.example.codcat.pandoraplayer.feature.main_screen;

import com.example.codcat.pandoraplayer.base.BasePresenter;
import javax.inject.Inject;


public class MainScreenPresenter<V extends MainScreenContract.View> extends BasePresenter<V> implements MainScreenContract.Presenter<V> {

    @Inject
    public MainScreenPresenter(V mMvpView) {
        super(mMvpView);
    }

}
