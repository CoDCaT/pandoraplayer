
package com.example.codcat.pandoraplayer.data.pojo.Responce;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptionalFeatures {

    @SerializedName("optionalFeature")
    @Expose
    private List<OptionalFeature> optionalFeature = null;

    public List<OptionalFeature> getOptionalFeature() {
        return optionalFeature;
    }

    public void setOptionalFeature(List<OptionalFeature> optionalFeature) {
        this.optionalFeature = optionalFeature;
    }

}
