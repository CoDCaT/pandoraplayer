
package com.example.codcat.pandoraplayer.data.pojo.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlatformVersionRange {

    @SerializedName("low")
    @Expose
    private String low;
    @SerializedName("high")
    @Expose
    private String high;

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

}
