package com.example.codcat.pandoraplayer.data.repository;


import com.example.codcat.pandoraplayer.App;
import com.example.codcat.pandoraplayer.api.ApiService;
import com.example.codcat.pandoraplayer.data.pojo.Responce.Response;
import com.example.codcat.pandoraplayer.data.pojo.login_pojo.Partner;
import com.example.codcat.pandoraplayer.utils.Helper;
import com.example.codcat.pandoraplayer.utils.LogUtils;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.realm.Realm;

@Singleton
public class AppRepository implements IRepository {

    @Inject ApiService apiService;
    @Inject Realm realm;
    private ArrayList currentListPhoto = new ArrayList();

    @Inject
    public AppRepository() {
        realm = App.repositoryComponent.getRealm();
        LogUtils.E("IS Realm attached to AppRepository: " + (realm != null));
    }

    @Override
    public Realm getRealm() {
        return realm;
    }

    @Override
    public Single<Response> getAuthData(String login, String password) {
//        return apiService.loginUser(new User(null, true, Helper.PASS2, Helper.LOGIN));
//        return apiService.checkLicensing("test.checkLicensing");
        return apiService.loginPartner("auth.partnerLogin", new Partner("5", "android-generic", password, login));

    }

    @Override
    public Single<Response> isAllowedLicense() {
        return apiService.checkLicensing("test.checkLicensing");
//            emitter.onSuccess();
    }
}
