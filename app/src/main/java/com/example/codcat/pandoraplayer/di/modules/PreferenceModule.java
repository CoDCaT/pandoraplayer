package com.example.codcat.pandoraplayer.di.modules;

import android.content.Context;

import com.example.codcat.pandoraplayer.data.repository.AppPreferences;
import com.example.codcat.pandoraplayer.data.repository.IPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferenceModule {

    private Context appContext;

    public PreferenceModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    IPreferences getDefaultInstance(){
        return new AppPreferences(appContext);
    }
}
