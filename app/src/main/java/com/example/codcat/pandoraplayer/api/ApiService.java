package com.example.codcat.pandoraplayer.api;


import com.example.codcat.pandoraplayer.data.pojo.Responce.Response;
import com.example.codcat.pandoraplayer.data.pojo.login_pojo.Partner;
import com.example.codcat.pandoraplayer.data.pojo.delete.ResponseLogin;
import com.example.codcat.pandoraplayer.data.pojo.login_pojo.User;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

//    Observable<String> getPhotoApi(@Query("photoreference") String photoreference);

    //Get city by API Google gecod
//    @GET("geocode/json?language=en&key=" + Helper.API_KEY_GOOGLE_BOOK)
//    Observable<List<Photo>> getPlacePhotoApi(@Query("latlng") String location);
//
//    //Get photo by API
//    @GET("place/photo?maxwidth=400&key=" + Helper.API_KEY_GOOGLE_BOOK)

    @Headers({
            "Content-Type: application/json",
            "X-CsrfToken: 123456 a7889b1c23",
//            "X-AuthToken:"
    })
    @POST("v1/auth/login")
    Single<ResponseLogin> loginUser(@Body User user);

    @Headers({
            "Content-Type: application/json",
//            "X-CsrfToken: 123456 a7889b1c23",
//            "X-AuthToken:"
    })
    @POST("services/json/")
    Single<Response> loginPartner(@Query("method") String method, @Body Partner partner);

    @Headers({
            "Content-Type: application/json",
    })
    @POST("services/json/")
    Single<Response> checkLicensing(@Query("method") String method);


}
