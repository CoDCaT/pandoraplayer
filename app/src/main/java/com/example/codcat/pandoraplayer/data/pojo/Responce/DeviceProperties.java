
package com.example.codcat.pandoraplayer.data.pojo.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceProperties {

    @SerializedName("optionalFeatures")
    @Expose
    private OptionalFeatures optionalFeatures;
    @SerializedName("followOnAdRefreshInterval")
    @Expose
    private Integer followOnAdRefreshInterval;
    @SerializedName("ooyala")
    @Expose
    private Ooyala ooyala;
    @SerializedName("adRefreshInterval")
    @Expose
    private Integer adRefreshInterval;
    @SerializedName("videoAdStartInterval")
    @Expose
    private Integer videoAdStartInterval;
    @SerializedName("shouldUseWebViewUserAgent")
    @Expose
    private Boolean shouldUseWebViewUserAgent;
    @SerializedName("videoAdUniqueInterval")
    @Expose
    private Integer videoAdUniqueInterval;
    @SerializedName("videoAdRefreshInterval")
    @Expose
    private Integer videoAdRefreshInterval;

    public OptionalFeatures getOptionalFeatures() {
        return optionalFeatures;
    }

    public void setOptionalFeatures(OptionalFeatures optionalFeatures) {
        this.optionalFeatures = optionalFeatures;
    }

    public Integer getFollowOnAdRefreshInterval() {
        return followOnAdRefreshInterval;
    }

    public void setFollowOnAdRefreshInterval(Integer followOnAdRefreshInterval) {
        this.followOnAdRefreshInterval = followOnAdRefreshInterval;
    }

    public Ooyala getOoyala() {
        return ooyala;
    }

    public void setOoyala(Ooyala ooyala) {
        this.ooyala = ooyala;
    }

    public Integer getAdRefreshInterval() {
        return adRefreshInterval;
    }

    public void setAdRefreshInterval(Integer adRefreshInterval) {
        this.adRefreshInterval = adRefreshInterval;
    }

    public Integer getVideoAdStartInterval() {
        return videoAdStartInterval;
    }

    public void setVideoAdStartInterval(Integer videoAdStartInterval) {
        this.videoAdStartInterval = videoAdStartInterval;
    }

    public Boolean getShouldUseWebViewUserAgent() {
        return shouldUseWebViewUserAgent;
    }

    public void setShouldUseWebViewUserAgent(Boolean shouldUseWebViewUserAgent) {
        this.shouldUseWebViewUserAgent = shouldUseWebViewUserAgent;
    }

    public Integer getVideoAdUniqueInterval() {
        return videoAdUniqueInterval;
    }

    public void setVideoAdUniqueInterval(Integer videoAdUniqueInterval) {
        this.videoAdUniqueInterval = videoAdUniqueInterval;
    }

    public Integer getVideoAdRefreshInterval() {
        return videoAdRefreshInterval;
    }

    public void setVideoAdRefreshInterval(Integer videoAdRefreshInterval) {
        this.videoAdRefreshInterval = videoAdRefreshInterval;
    }

}
