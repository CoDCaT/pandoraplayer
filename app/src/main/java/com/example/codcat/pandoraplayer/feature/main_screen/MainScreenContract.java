package com.example.codcat.pandoraplayer.feature.main_screen;


import com.example.codcat.pandoraplayer.base.MvpPresenter;
import com.example.codcat.pandoraplayer.base.MvpView;

public interface MainScreenContract {

    interface View extends MvpView{

    }

    interface Presenter<V extends MvpView> extends MvpPresenter<V> {

    }
}
