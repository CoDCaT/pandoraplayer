package com.example.codcat.pandoraplayer.di.modules;

import com.example.codcat.pandoraplayer.data.repository.AppRepository;
import com.example.codcat.pandoraplayer.data.repository.IRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppRepositoryModule {

    @Provides
    @Singleton
    IRepository appRepository(){
        return new AppRepository();
    }
}
