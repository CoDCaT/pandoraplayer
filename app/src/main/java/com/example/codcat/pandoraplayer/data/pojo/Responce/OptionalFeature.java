
package com.example.codcat.pandoraplayer.data.pojo.Responce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptionalFeature {

    @SerializedName("feature")
    @Expose
    private String feature;
    @SerializedName("enabled")
    @Expose
    private String enabled;
    @SerializedName("platformVersionRange")
    @Expose
    private PlatformVersionRange platformVersionRange;
    @SerializedName("productVersionRange")
    @Expose
    private ProductVersionRange productVersionRange;

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public PlatformVersionRange getPlatformVersionRange() {
        return platformVersionRange;
    }

    public void setPlatformVersionRange(PlatformVersionRange platformVersionRange) {
        this.platformVersionRange = platformVersionRange;
    }

    public ProductVersionRange getProductVersionRange() {
        return productVersionRange;
    }

    public void setProductVersionRange(ProductVersionRange productVersionRange) {
        this.productVersionRange = productVersionRange;
    }

}
