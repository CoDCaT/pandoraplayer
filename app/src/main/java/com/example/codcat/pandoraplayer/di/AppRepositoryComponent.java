package com.example.codcat.pandoraplayer.di;

import com.example.codcat.pandoraplayer.data.repository.AppRepository;
import com.example.codcat.pandoraplayer.data.repository.IPreferences;
import com.example.codcat.pandoraplayer.di.modules.AppRepositoryModule;
import com.example.codcat.pandoraplayer.di.modules.PreferenceModule;
import com.example.codcat.pandoraplayer.di.modules.RealmModule;
import com.example.codcat.pandoraplayer.di.modules.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import io.realm.Realm;

@Singleton
@Component(modules = {
        AppRepositoryModule.class,
        RealmModule.class,
        PreferenceModule.class,
        RetrofitModule.class})
public interface AppRepositoryComponent {
    AppRepository getRepository();
    Realm getRealm();
    IPreferences getAppPreferences();
//    Retrofit getRetrofit();
}
