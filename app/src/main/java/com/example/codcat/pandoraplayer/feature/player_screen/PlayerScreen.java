package com.example.codcat.pandoraplayer.feature.player_screen;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.codcat.pandoraplayer.R;

import dagger.android.support.DaggerAppCompatActivity;

public class PlayerScreen extends DaggerAppCompatActivity implements PlayerScreenContract.View{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_screen);
    }

    @Override public void showLoading() {

    }

    @Override public void hideLoading() {

    }

    @Override public void onError(@NonNull String message) {

    }

    @Override public void showMessage(@NonNull String message) {

    }
}
